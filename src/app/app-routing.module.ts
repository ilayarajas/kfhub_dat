// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

// import {HomepageComponent} from './components/homepage/homepage.component';
// import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';

// const routes: Routes = [
//   { path: '', redirectTo: '/home', pathMatch: 'full' },
//   {path:'home', component: HomepageComponent},
//   {path: 'upload', component: UploadDocumentsComponent}
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }


import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// CUSTOM COMPONENTS

import {HomepageComponent} from './components/homepage/homepage.component';
import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      { path: 'home', component: HomepageComponent },
      { path: 'upload', component: UploadDocumentsComponent},
      //{ path: '**', redirectTo: 'upload', pathMatch: 'full' },
    ], { useHash: true })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
