import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  XhrFactory,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {

  private fileUploadRequest: any;

  constructor(private http: HttpClient) {}

  // fetching data from server
  public fetchData(url: string) {
    return this.http.get(url, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'Accept': 'application/vnd.openxmlformats',
        'Content-Type': 'application/vnd.openxmlformats',
        'AuthToken': 'abcabc'
      })
    });
  }

  // get data from api
  public create(body: any, type: string): Blob {
    return new Blob([body], {
      type,
    });
  }

  public createXLSX(body: any): Blob {
    return this.create(body, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  }
  public download(blob: Blob, fileName: string): void {
    const downloadLink = document.createElement('a');
    if (window.navigator.msSaveOrOpenBlob) {
      downloadLink.onclick = () => {
        window.navigator.msSaveOrOpenBlob(blob, fileName);
      };
      // this.click(downloadLink);
      downloadLink.click();
    } else {
      downloadLink.href = window.URL.createObjectURL(blob);
      downloadLink.setAttribute('download', fileName);
      // this.click(downloadLink);
      downloadLink.click();
      window.URL.revokeObjectURL(downloadLink.href);
    }
  }

  public uploadFile(documentFileArray: File[]): Promise < any > {
    return new Promise((resolve, reject) => {
      const formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();
      for (let i = 0; i < documentFileArray.length; i++) {
        formData.append('file', documentFileArray[i], documentFileArray[i].name);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            resolve(JSON.parse(xhr.response));
          }
        }
      };
      xhr.open('POST', 'https://stvte805si.execute-api.us-east-1.amazonaws.com/Dev/file/upload', true);
      xhr.setRequestHeader('Accept', 'application/json');
      // xhr.setRequestHeader('Content-Type','multipart/form-data');
      xhr.setRequestHeader('AuthToken', 'abcabc');
      xhr.setRequestHeader('enctype', 'multipart/form-data');
      xhr.send(formData);
      this.fileUploadRequest = xhr;
    });
  }

  public cancelFileUpload() {
    this.fileUploadRequest.abort();
  }
  /**
   * presignUpload
   */
  public getPresignUploadURL() {
    const url = 'https://quya1qdhg9.execute-api.us-east-1.amazonaws.com/Prod/v1/hrms/dataaggregation/datasources';
    const headers = new HttpHeaders(
        {
            // 'Content-Type': 'text/plain',
        },
    );
    const presignType = 'putObject';
    const body = { };
    const options = {
        headers,
    };
    const req = new HttpRequest('POST', url, body, options);
    return this.http.request(req);

  }
  /**
   * upload to s3
   */
  public uploadfileAWSS3(fileUploadURL, contenttype, file): Observable<any> {
    const headers = { 'Content-Type': contenttype };
    const options = {
        Headers: headers,
        reportProgress: true, // This is required for track upload process
    };
    const req = new HttpRequest('PUT', fileUploadURL, file, options);
    return this.http.request(req);
  }
}