import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import {  HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';
import { AppService } from './app.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule, MatProgressBarModule } from '@angular/material';
// import { Http, RequestOptions, Headers } from '@angular/http';

// for drag-drop-upload feature
import { ngfModule, ngf } from "angular-file";



@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    UploadDocumentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatProgressBarModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ngfModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
