import {
  Component,
  OnInit
} from '@angular/core';
import {
  AppService
} from '../../app.service';
import {
  timer
} from 'rxjs/observable/timer';


@Component({
  selector: 'app-upload-documents',
  templateUrl: './upload-documents.component.html',
  styleUrls: ['./upload-documents.component.scss']
})
export class UploadDocumentsComponent implements OnInit {

  public documentFileName = '';
  public documentFileArray: any;
  public documentfile: Array < File > ;
  public showProgressBar: boolean = false;
  public uploadSuccess: boolean = false;
  public uploadFailed: boolean = false;
  public uploadFileStatus: boolean = true;
  public isFirstUpload = true; // using it for resetting progressbar
  public progressValue;
  public progressBar;
  curSec: number = 0;
  accept = 'application/vnd.openxmlformats';
  private files: File[] = [];
  hasBaseDropZoneOver: boolean = false;

  constructor(private appService: AppService) {
    this.documentFileArray = new Array < File > ();
  }

  titleText = 'UPLOAD A NEW DATA SOURCE';
  stepCountText = 'Step 1 of 2: Select your file and upload';
  uploadStatus: any = 'Uploading Spreadsheet…';
  uploadProgress = '0%'; // must be between 0-100

  fileToUpload: File;

  ngOnInit() {
    // document.body.style.overflow = "hidden";
    // document.body.style.height: "100vh";
    // console.log('upload screen....');
     }

  nextStep() {
    this.titleText = 'DEFINE YOUR DATA SOURCE';
    this.stepCountText = 'Step 2 of 2: Give us some basic information about your data source to get started.';

    document.querySelector('.step-counter .step:nth-child(2)').classList.add('done');
    document.querySelector('.step-1').classList.add('d-none');
    document.querySelector('.step-2').classList.remove('d-none');
  }


  // upload document via form
  async uploadDocument(fileInput: any) {
    console.log(fileInput);
    this.uploadSuccess = false;
    this.uploadFailed = false;
    this.uploadFileStatus = false;
    this.showProgressBar = true;
    if(!this.isFirstUpload){
      this.resetProgressBar();
    }
    this.progress();
    this.isFirstUpload = false;

    const FileList: FileList = fileInput;
    this.documentFileName = fileInput[0].name;
    if (this.documentFileName.substring(this.documentFileName.length - 5) === '.xlsx') {
      this.documentFileArray.length = 0;
      for (let i = 0, length = FileList.length; i < length; i++) {
        this.documentFileArray.push(FileList[i]);
      }
      const data = await this.appService.uploadFile(this.documentFileArray);
      if (data && data.status && data.status.responseCode === 200) {
        this.progressValue = 100;
        this.showProgressBar = false;
        this.uploadSuccess = true;
      } else {
        this.uploadFailed = true;
        this.showProgressBar = false;
      }
      console.log(data);
    } else {
      // show error
    }
  }

  cancelUpload() {
    this.appService.cancelFileUpload();
    this.showProgressBar = false;
    this.uploadSuccess = false;
    this.uploadFailed = false;
    this.uploadFileStatus = true;
  }

  progress() {
    const source = timer(1000, 100);
    this.progressBar = source.subscribe(val => {
      this.progressValue = val;
      if (val === 95) {
        // this.showProgressBar = false;
        this.progressBar.unsubscribe();
      }
    });
  }

  resetProgressBar() {
    this.progressBar.unsubscribe();
    this.progressValue = 0;
  }

  uploadDocumentByDrop(fileArray) {
    this.files = fileArray[0];
    this.uploadDocument(fileArray);
  }

  uploadDocumentByClick(event) {
    // this.uploadDocument(event.target.files);
        var reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            var file = event.target.files[0];
            reader.readAsArrayBuffer(file);
            reader.onload = () => {
                console.log('filename', file.name);
                console.log('filetype', file.type);
                console.log('value', reader.result);
                // const goodEntry = this.getGoodEntry();
                // const newFileName = this.getCurrentFileName(goodEntry);
                // this.reUpload(newFileName, reader.result, file.type);
            };
            console.log('####', reader.result);
        }

    this.appService.getPresignUploadURL()
    .subscribe((data) => {
      if (data['body']) {
        console.log('>>>>presign url::', data['body'].data.s3UplaodUrl);
        const uploadUrl = data['body'].data.s3UplaodUrl;
        localStorage.setItem('dataSourceId', data['body'].data.dataSourceId);
        console.log('>>>>>>filedata:', reader.result);
        this.appService.uploadfileAWSS3(uploadUrl, file.type, reader.result)
        .subscribe((data) => {
          console.log('>>>>>upload response:', data);
        });
      }



    });
  }
}
