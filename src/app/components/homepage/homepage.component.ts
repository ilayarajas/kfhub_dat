import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';

@Component({
  selector: 'kf-app-homepage',
  templateUrl: './homepage.html',
})

export class HomepageComponent implements OnInit {

  selectedCountry: string;
  data;

  constructor(private appService: AppService) { }

  ngOnInit() {
    console.log('home screen....');
  }

  // setting value for selected country
  // which will enable Download Button
  activateDownloadButton(event) {
    document.querySelector('.mat-form-field-label-wrapper').classList.add('d-none');
    this.selectedCountry = event.value;
  }

  // downlaoding template file
  downloadTemplate(filename) {
    this.appService.fetchData('https://stvte805si.execute-api.us-east-1.amazonaws.com/Dev/file/download/template')
      .subscribe(response => {
        this.data = response;
        console.log(this.data);
        var a = document.createElement("a");
        a.href = response['data']['templateUrl'];
        a.setAttribute("download", filename);
        a.click();
      });
  }

  showPopUp(event){
    document.querySelector('.pop-up__wrapper').classList.remove('d-none');
    document.querySelector('body').classList.add('overflow-hidden');
  }
  hidePopUp(event){
    if(event.target.classList.contains('pop-up')){
      return;
    }
    document.querySelector('.pop-up__wrapper').classList.add('d-none');
    document.querySelector('body').classList.remove('overflow-hidden');
  }

}
